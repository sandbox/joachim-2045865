Entityform block
================

This provides an extra setting on Entityform types to expose the entity form as a block.
